// Kuinka monta mittausta otetaan kalibrointia varten
#define VAKIOTERMISAMPLES 16384

double vakioTermi = 0;

// Tähän muuttujaan tallennetaan integraali
double alusta_asti = 0;

const double eps = 1e-6;
double X = 0;

// Askellaskin
unsigned int ind = 0;

inline float convertAnalogToVolt(int analogValue)
{
  return 5.0 * (analogValue - vakioTermi)/1023.0;
}

// Tämä funktio kalibroi jännitemittarin
void calibrateVakioTermi() {
  long acc = 0;
  for (int i = 0; i < VAKIOTERMISAMPLES; ++i) {
    acc += analogRead(A0);
  }
  vakioTermi = acc / (double) VAKIOTERMISAMPLES;
}

void setup() {
  Serial.begin(115200);

  calibrateVakioTermi();
}

void loop() {
  int analog = analogRead(A0);

  double val = convertAnalogToVolt(analog);
  alusta_asti = alusta_asti*(1-eps) + 1e4*eps*val;
  if (alusta_asti < -8) alusta_asti = -8;
  if (alusta_asti > 8) alusta_asti = 8;

  if (ind % 64 == 0) {
    // Tulostaa mitatun jännitteen (sininen)
    Serial.print(3*convertAnalogToVolt(analog));
    Serial.print(" ");

    // Tulostaa integraalin (punainen)
    Serial.print(alusta_asti);
    Serial.println();
  }
  ind++;
}
